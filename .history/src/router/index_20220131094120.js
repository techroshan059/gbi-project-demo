import Vue from "vue";
import Router from "vue-router";

Vue.use(Router);

export default new Router({
  routes: [
    {
      path: "",
      name: "home.page",
      component: () => import("@/views/Home")
    },
    {
      path: "/project-list",
      name: "project.list.page",
      component: () => import("@/views/Project/List")
    },
    {
      path: "/project/details/:project_id",
      name: "project.details.page",
      component: () => import("@/views/Project/Details")
    },
    {
      path: "/about-us",
      name: "page.about.us",
      component: () => import("@/views/AboutUs")
    }
  ]
});
