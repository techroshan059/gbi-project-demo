import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./store";
import "./registerServiceWorker";
// eslint-disable-next-line no-unused-vars
import * as $ from "jquery";

const MyPlugin = {
  install(Vue) {
    (Vue.prototype.globalOpenLoginModal = () => {
      // eslint-disable-next-line no-undef
      window.$("#signup_modal_kw").modal("hide");
      // eslint-disable-next-line no-undef
      window.$("#login_modal_kw").modal("show");
    }),
      (Vue.prototype.globalOpenSignupModal = () => {
        // eslint-disable-next-line no-undef
        window.$("#login_modal_kw").modal("hide");
        // eslint-disable-next-line no-undef
        window.$("#signup_modal_kw").modal("show");
      });
  }
};
Vue.use(MyPlugin);

Vue.config.productionTip = false;
new Vue({
  router,
  store,
  render: (h) => h(App)
}).$mount("#app");
