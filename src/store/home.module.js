// import { FETCH_ARTICLES, FETCH_TAGS } from "./actions.type";
import { SET_HOME_INFORMATION_TAB_NAME } from "@/store/mutations.type";

const state = {
  informationTabName: "tab-1"
};

const getters = {
  getInfomationTabName(state) {
    return state.informationTabName;
  }
};

const actions = {};

/* eslint no-param-reassign: ["error", { "props": false }] */
const mutations = {
  [SET_HOME_INFORMATION_TAB_NAME](state, name) {
    state.informationTabName = name;
  }
};

export default {
  state,
  getters,
  actions,
  mutations
};
